#pragma once
#include "ram.h"
#include "cpu.h"

int compute(BUFFER& x) {
    int sum = 0;
    for (int i = 0; i < 8; i++) {
        sum += x.num[i];
    }
    return sum;
}