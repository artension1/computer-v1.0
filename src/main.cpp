#include<iostream>
#include "ram.h"
#include "gpu.h"
#include "disk.h"
#include "cpu.h"

int main() {

    std::string command;

    BUFFER buf;
    BUFFER& y = buf;
    BUFFER x;

    for (;;) {
        std::cout << "Input command" << std::endl;
        std::cin >> command;
        if (command == "input") {
            y = read();
        }
        else if (command == "display") {
            output(buf);
        }
        else if (command == "load") {
            y = load(x);
        }
        else if (command == "save") {
            save(buf);
        }
        else if (command == "sum") {
            std::cout << compute(buf) << std::endl;
        }
        else if (command == "exit") {
            return 0;
        }
        else {
            std::cout << "unknown command" << std::endl;
        }
    }
}